from django.http import HttpResponse
from django.shortcuts import render
from listing.models import drugs


def hello(request): 
    return HttpResponse('Welcome Par ici')

def about(request): 
    return HttpResponse('A propos de PharmaHome')

def listing(request):
    drugs_list = drugs.objects.all()
    return HttpResponse(f"""
    <h1>Liste des médicaments</h1>
    <ul>
        <li>Nom : {drugs_list[0].drug_name}</li>
        <li>Laboratoire : {drugs_list[0].drug_laboratory}</li>
        <li>Prix : {drugs_list[0].drug_price}</li>
        <li>Quantité : {drugs_list[0].drug_quantity}</li>
        <li>Date d'expiration : {drugs_list[0].drug_expiration_date}</li>
        <li>Date de fabrication : {drugs_list[0].drug_manufacture_date}</li>
        <li>Date d'achat : {drugs_list[0].drug_purchase_date}</li>
        <li>Description : {drugs_list[0].drug_description}</li>
    </ul>
    """)

def contact(request):
    return HttpResponse('Ceci est la page de contact')

