from pydoc import describe
from datetime import datetime
from django.utils import timezone
from django.db import models

class drugs(models.Model):
    drug_name = models.CharField(max_length=100)
    drug_laboratory = models.CharField(max_length=100)
    drug_price = models.IntegerField()
    drug_quantity = models.IntegerField()
    drug_expiration_date = models.DateField(null=True, blank=True)
    drug_manufacture_date = models.DateField(null=True, blank=True)
    drug_purchase_date = models.DateField(null=True, blank=True)
    drug_description = models.TextField(max_length=1000)
    